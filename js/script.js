
angular.module("CadClientes", [] );

angular.module("CadClientes").controller("CadClienteCtrl", function($scope){
	$scope.message = "Cadastro de Clientes";
	$scope.clientes = [
		{nome: "João", codigo: "1", tipo: "Pessoa Física", data: new Date()},
		{nome: "José", codigo: "2" , tipo: "Pessoa Física", data: new Date()}
		
	];

	$scope.tipoCliente = [
		{tipo: "Pessoa Física", codigo:"01" },
		{tipo: "Pessoa Jurídica", codigo:"02" },
		{tipo: "Sócios", codigo:"03" },
	];


	$scope.adicionarCliente = function( cliente ){ 
		$scope.clientes.push(angular.copy(cliente));
		delete $scope.cliente;
	};

	$scope.apagarCliente = function( clientes ){ 
		$scope.clientes = clientes.filter(function(cliente){
			if(!cliente.selecionado) return cliente;
		});
	};

	$scope.temClienteSelecionado = function( clientes ){ 
		return clientes.some(function(cliente){
			return cliente.selecionado;
		});
	};
});

// Diretivas

angular.module("CadClientes").directive("diralert", function() {
	return {templateUrl: "alerta.html"};
});